require 'books/record'
require 'rest-client'

module Books
  class Author < Record
    attrs :id, :name, :search_name
    has_many :books

    def initialize(*args)
      super
      @result_cache = nil
    end

    def name_with_link
      link = "https://en.wikipedia.org/wiki/#{URI.encode(@data['name'])}"
      begin
        @result_cache = RestClient.get(link).code if @result_cache.nil?
      rescue RestClient::NotFound
        @result_cache = 404
      end

      if @result_cache != 404
        return "#{name} (#{link})"
      else
        return name
      end
    end
  end

  class Book < Record
    attrs :id, :title, :year
    has_many :authors
    belongs_to :language
    belongs_to :publisher
  end

  class Language < Record
    attrs :id, :language
    has_many :books
  end

  class Publisher < Record
    attrs :id, :publisher
    has_many :books
  end

  class BookAuthor < Record
    def add_links(database)
      super
      book = database.table(:books).search(:id => @data['book_id']).first
      author = database.table(:authors).search(:id => @data['author_id']).first
      book.authors << author
      author.books << book
    end
  end
end
