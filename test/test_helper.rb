require 'simplecov'
$LOAD_PATH << File.join(File.dirname(__FILE__), '..', 'lib/')

SimpleCov.start do
  add_filter "/test/"
end
SimpleCov.minimum_coverage 95

require 'minitest/autorun'
