books.rb: Search books by different attributes
==============================================

Úkol do předmětu Ruby PV249.

Zadání
------

Vašim úkolem bude naspat testy pro tento projekt. Jedná se o vzorové řešení prvního úkolu s drobným rozšířením, které načítá informace z wikipedie. Při výpisu knihy se dotážeme wikipedia.org, zda existuje stránka se jménem autora. Pokud ano, je do závorky za autorovo jméno přidán odkaz na wikipedii.

Testy se mají chovat následovně následovně:

1. testy se pustí pomocí příkazu `rake test`

2. test coverage musí být nejméně 95%

3. testy nesmí komunikovat s žádným vnějším serverem, musí běžět i na počítači bez přístupu k Internetu

4. v exstujícím kódu se nesmí provádět žádné změny, je však možné přidávat libovolné gemy v Gemfile a upravovat libovolné soubory v test/

Podmínky pro uznání řešení:
---------------------------

1. všechny testy procházejí
2. je splněn limit pro test coverage
3. každá třída má svůj unit test s výjimkou modelů Book, Language, Publisher. Pro modely Author a BookRecord je test vyžadován, stejně jako pro společného předka Record.

`bundle exec rake test` spustí testy. Před prvním spuštěním nezapomeňte
na `bundle install`.


Může se hodit
-------------

* [Studijní materiály](https://is.muni.cz/auth/el/1433/podzim2017/PV249/um/programming-ruby-pv249.pdf)

Odevzdání
---------

* Do 23. 11. 2017 (včetně)
* Konzultace k úkolu na cvičení 13. 10. 2017
* Způsob odevzdání: skrze gitlab, přes který dostanete komentář k řešení

